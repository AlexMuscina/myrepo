import React from "react";

function Step2() {
  return (
    <>
      <div className={classes.InputContainer}>
        <input
          required
          id="companyName"
          autoComplete="off"
          type="text"
          placeholder=" "
          name="companyName"
          value={form.companyName}
          onChange={updateForm}
        />
        <label htmlFor="companyName" className={classes.LabelName}>
          <span className={classes.ContentName}>Company Name</span>
        </label>
      </div>
    </>
  );
}

export default Step2;
