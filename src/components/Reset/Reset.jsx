import React, { useState } from "react";
import Card from "../UI/Card/Card";
import CodeCheckForm from "../CodeCheckForm/CodeCheckForm";
import NewPassForm from "../NewPassForm/NewPassForm";

function Reset({ history }) {
  const [nextStep, setNextStep] = useState(false);
  const [error, setError] = useState(false);
  const [showPass, setShowPass] = useState(false);
  const [form, setForm] = useState({
    newPass: "",
    confirmedPass: "",
    signOut: true,
  });

  const updateForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  let code = 123456;
  const codeCheck = (values) => {
    Number(code) !== Number(values) ? setError(true) : setError(false);
    Number(code) !== Number(values) ? setNextStep(false) : setNextStep(true);
  };

  return (
    <Card>
      <h1 className="h1">Update your password</h1>
      {nextStep ? null : (
        <CodeCheckForm
          nextStep={nextStep}
          error={error}
          setError={setError}
          codeCheck={codeCheck}
          history={history}
        />
      )}
      {nextStep ? (
        <NewPassForm
          nextStep={nextStep}
          form={form}
          setForm={setForm}
          updateForm={updateForm}
          setShowPass={setShowPass}
          showPass={showPass}
          // checkPassword={(e) => checkPassword(e)}
        />
      ) : null}
    </Card>
  );
}

export default Reset;
