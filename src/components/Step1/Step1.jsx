import React from "react";
import classes from "./Step1.module.scss";
import ToggleVisibilityIcon from "../UI/ToggleVisibilityIcon/ToggleVisibilityIcon";
import { Link } from "react-router-dom";

function Step1({ form, setForm, updateForm, showPass, setShowPass }) {
  return (
    <>
      <div className={classes.InputContainer}>
        <input
          required
          id="email"
          autoComplete="off"
          type="email"
          placeholder=" "
          name="email"
          value={form.email}
          onChange={updateForm}
        />
        <label htmlFor="email" className={classes.LabelName}>
          <span className={classes.ContentName}>Email address</span>
        </label>
      </div>
      <div className={classes.Row}>
        <div className={classes.InputContainer} style={{ marginRight: "2rem" }}>
          <input
            required
            id="fName"
            autoComplete="off"
            type="text"
            placeholder=" "
            name="fName"
            value={form.fName}
            onChange={updateForm}
          />
          <label htmlFor="fName" className={classes.LabelName}>
            <span className={classes.ContentName}>First Name</span>
          </label>
        </div>
        <div className={classes.InputContainer}>
          <input
            required
            id="lName"
            autoComplete="off"
            type="text"
            placeholder=" "
            name="lName"
            value={form.lName}
            onChange={updateForm}
          />
          <label htmlFor="lName" className={classes.LabelName}>
            <span className={classes.ContentName}>Last Name</span>
          </label>
        </div>
      </div>
      <div className={classes.InputContainer}>
        <input
          required
          id="password"
          type={showPass ? "text" : "password"}
          autoComplete="off"
          // Placeholder " " pentru a putea folosi :not(:placeholder-shown)) in SCSS pentru a putea face animatia pentru placeholder
          placeholder=" "
          onChange={updateForm}
          name="password"
          value={form.password}
        />
        <label htmlFor="password" className={classes.LabelName}>
          <span className={classes.ContentName}>Password</span>
        </label>
        <ToggleVisibilityIcon
          toggleVisibility={() => setShowPass(!showPass)}
          showPass={showPass}
        />
      </div>
      <div className={classes.Privacy}>
        <div className={classes.CheckboxWrapper}>
          <input
            id="privacy"
            type="checkbox"
            name="privacyPolicy"
            checked={form.privacyPolicy}
            onChange={(e) => {
              setForm({ ...form, privacyPolicy: !form.privacyPolicy });
            }}
          />
          <label className={`system ${classes.PrivacyLabel}`} htmlFor="privacy">
            {" "}
            GuidefAI may keep me informet with personalized emails about
            products and services. See our{" "}
            <Link className={classes.LinkSystem} to="/privacy-policy">
              Privacy Policy
            </Link>{" "}
            for more details or to opt-out at any time.
          </label>
        </div>
        <p style={{ marginTop: "1rem" }} className="system">
          By clicking Create account, I agree that I have read and accepted the{" "}
          <Link className={classes.LinkSystem} to="/terms">
            Terms of Use
          </Link>{" "}
          and{" "}
          <Link className={classes.LinkSystem} to="/privacy-policy">
            Privacy Policy
          </Link>
          .
        </p>
      </div>
    </>
  );
}

export default Step1;
