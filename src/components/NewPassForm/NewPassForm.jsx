import React from "react";
import Button from "../UI/Button/Button";
import ToggleVisibilityIcon from "../UI/ToggleVisibilityIcon/ToggleVisibilityIcon";
import classes from "./NewPassForm.module.scss";
import variables from "../../utilities/color-variables.module.scss";

function NewPassForm({
  showPass,
  form,
  updateForm,
  setShowPass,
  setForm,
  checkPassword,
}) {
  return (
    <>
      <div className={classes.Profile}>
        <div className={classes.ProfilePic}></div>
        <div>
          <p className={classes.TextSmall}>Personal account</p>
          <p className="body-text">lorem.ipsum@gmail.com</p>
        </div>
      </div>
      <form className={classes.Form}>
        <div className={classes.InputContainer}>
          <input
            required
            id="newPass"
            type={showPass ? "text" : "password"}
            autoComplete="off"
            // Placeholder " " pentru a putea folosi :not(:placeholder-shown)) in SCSS pentru a putea face animatia pentru placeholder
            placeholder=" "
            name="newPass"
            value={form.newPass}
            onChange={updateForm}
          />
          <label htmlFor="password" className={classes.LabelName}>
            <span className={classes.ContentName}>New password</span>
          </label>
          <ToggleVisibilityIcon
            toggleVisibility={() => setShowPass(!showPass)}
            showPass={showPass}
          />
        </div>
        <div className={classes.InputContainer}>
          <input
            required
            id="confirmedPass"
            type={showPass ? "text" : "password"}
            autoComplete="off"
            // Placeholder " " pentru a putea folosi :not(:placeholder-shown)) in SCSS pentru a putea face animatia pentru placeholder
            placeholder=" "
            name="confirmedPass"
            value={form.confirmedPass}
            onChange={updateForm}
          />
          <label htmlFor="password" className={classes.LabelName}>
            <span className={classes.ContentName}>Confirm new password</span>
          </label>
          <ToggleVisibilityIcon
            toggleVisibility={() => setShowPass(!showPass)}
            showPass={showPass}
          />
        </div>
        <div className={classes.CheckboxWrapper}>
          <input
            className={classes.CheckboxInput}
            id="signOut"
            type="checkbox"
            name="signOut"
            checked={form.signOut}
            onChange={(e) => {
              setForm({ ...form, signOut: !form.signOut });
            }}
          />
          <label
            className={`${classes.SignOutLabel} body-text`}
            htmlFor="signOut"
          >
            Sign out of all active logins
          </label>
        </div>
        <div className={classes.BtnContainer}>
          <Button
            text="Update password"
            bgColor={variables.transparent}
            border
            color={variables.blue1}
            width="initial"
            weight="bold"
            click={checkPassword}
          />
        </div>
      </form>
    </>
  );
}

export default NewPassForm;
