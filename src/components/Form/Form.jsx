// import React from "react";
// import { useForm } from "react-hook-form";

// function Form({ login }) {
//   const {
//     register,
//     handleSubmit,
//     // watch,
//     formState: { errors },
//   } = useForm();

//   const onSubmit = (data) => console.log(data);

//   return (
//     <form onSubmit={handleSubmit(onSubmit)}>
//       {login && (
//         <>
//           <input id="email" name="email" type="email" {...register("email")} />
//           <label htmlFor="email"> Email Address</label>
//           <input
//             id="password"
//             name="password"
//             type="password"
//             {...register("password", { required: true, minLength: 8 })}
//           />
//           <label htmlFor="password"> Password</label>
//           {errors.password && <span>password too short</span>}
//         </>
//       )}
//       <button type="submit">Submit</button>
//     </form>
//   );
// }

// export default Form;
