import React from "react";
import classes from "./CodeCheckForm.module.scss";
import variables from "../../utilities/color-variables.module.scss";
import Button from "../UI/Button/Button";
import ReactCodeInput from "react-verification-code-input";

function CodeCheckForm({ error, setError, codeCheck, history }) {
  return (
    <>
      <h5 className="h5">
        Enter the code we have just sent to{" "}
        <span style={{ fontWeight: "bold" }}>lorem.ipsum@gmail.com</span>
      </h5>
      <div className={classes.CodeInputContainer}>
        <ReactCodeInput
          onComplete={(values) => codeCheck(values)}
          onChange={() => setError(false)}
          fieldHeight={80}
          fieldWidth={50}
          className={!error ? null : "error"}
        />
        {!error ? null : (
          <p className={`system ${classes.ErrorMsg}`}>&#x2717; Invalid code</p>
        )}
      </div>
      <div className={classes.Row}>
        <Button
          text="Back"
          bgColor={variables.transparent}
          color={variables.blue1}
          width={"initial"}
          weight={"bold"}
          border
          click={() => history.goBack()}
        />
        <Button
          text="Resend Code"
          bgColor={variables.transparent}
          color={variables.blue1}
          width={"initial"}
          weight={"bold"}
          border
        />
      </div>
    </>
  );
}

export default CodeCheckForm;
