import React from "react";
import LoginForm from "../LoginForm/LoginForm";
import Reset from "../Reset/Reset";
import CreateAccountForm from "../CreateAccountForm/CreateAccountForm";
import Logo from "../UI/Logo/Logo";
import classes from "./LandingPage.module.scss";
import { Switch, Route } from "react-router-dom";
// import Form from "../Form/Form";

function LandingPage() {
  return (
    <div className={classes.Landing}>
      <Logo />
      <Switch>
        <Route exact path="/" component={LoginForm} />
        <Route path="/reset-password" component={Reset} />
        <Route path="/create-account" component={CreateAccountForm} />
      </Switch>
    </div>
  );
}

export default LandingPage;
