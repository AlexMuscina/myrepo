import React, { useState } from "react";
import Card from "../UI/Card/Card";
import classes from "./LoginForm.module.scss";
import variables from "../../utilities/color-variables.module.scss";
import Button from "../UI/Button/Button";
import ToggleVisibilityIcon from "../UI/ToggleVisibilityIcon/ToggleVisibilityIcon";
import Switch from "../UI/Switch/Switch";
import google from "../../assets/images/google.svg";
import facebook from "../../assets/images/facebook.svg";
import apple from "../../assets/images/apple.svg";
import { Link } from "react-router-dom";

function LoginForm() {
  const [showPass, setShowPass] = useState(false);
  const [isToggled, setIsToggled] = useState(true);
  const [form, setForm] = useState({
    email: "",
    password: "",
    signedIn: true,
  });

  const updateForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const updateToggle = (e) => {
    setIsToggled(!isToggled);
    let signedIn = form.signedIn;
    setForm({ ...form, signedIn: !signedIn });
  };

  return (
    // Folosim card component pentru look-ul overall
    <Card>
      <h1 className="h1">Sign In</h1>
      <div className={classes.CardText}>
        <h5 className={classes.H5}>New user?</h5>
        <Link to="/create-account" className={classes.LinkH5}>
          Create an account
        </Link>
      </div>
      <form className={classes.Form}>
        <div className={classes.InputContainer}>
          <input
            required
            id="email"
            autoComplete="off"
            type="email"
            placeholder=" "
            name="email"
            onChange={updateForm}
            value={form.email}
          />
          <label htmlFor="email" className={classes.LabelName}>
            <span className={classes.ContentName}>Email address</span>
          </label>
        </div>
        <div className={classes.InputContainer}>
          <input
            required
            id="password"
            type={showPass ? "text" : "password"}
            autoComplete="off"
            placeholder=" "
            name="password"
            value={form.password}
            onChange={updateForm}
          />
          <label htmlFor="password" className={classes.LabelName}>
            <span className={classes.ContentName}>Password</span>
          </label>
          <ToggleVisibilityIcon
            toggleVisibility={() => setShowPass(!showPass)}
            showPass={showPass}
          />
        </div>
        <div className={classes.Row}>
          <div className={classes.SwitchContainer}>
            <Switch
              isToggled={isToggled}
              onToggle={updateToggle}
              name="signedIn"
            />
            <p className="body-text">Stay signed in</p>
          </div>
          <Button
            text="Login"
            bgColor={variables.transparent}
            color={variables.blue1}
            border
            width="initial"
            weight="bold"
          />
        </div>
        <div className={classes.HrLine}>
          <p>Or</p>
        </div>
      </form>
      <div className={classes.SocialMediaContainer}>
        <Button
          bgColor={variables.transparent}
          color={variables.gray1}
          text="Continue with Google"
          border
          width="100%"
          weight="regular"
          icon={google}
        />
        <Button
          bgColor={variables.blue1}
          color={variables.gray4}
          text="Continue with Facebook"
          width="100%"
          weight="regular"
          icon={facebook}
        />
        <Button
          bgColor={variables.gray1}
          color={variables.gray4}
          text="Continue with Apple"
          width="100%"
          weight="regular"
          icon={apple}
        />
      </div>
      <Link to="/reset-password" className={classes.LinkBody}>
        Reset your password
      </Link>
    </Card>
  );
}

export default LoginForm;
