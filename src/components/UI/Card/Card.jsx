import React from "react";
import classes from "./Card.module.scss";

function Card(props) {
  return <div className={classes.CardBody}>{props.children}</div>;
}

export default Card;
