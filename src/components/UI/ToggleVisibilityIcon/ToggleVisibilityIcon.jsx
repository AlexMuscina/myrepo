import React from "react";
import eye from "../../../assets/images/eye.svg";
import eyeCrossed from "../../../assets/images/eye-crossed.svg";
import classes from "./ToggleVisibilityIcon.module.scss";

function ToggleVisibilityIcon({ showPass, toggleVisibility }) {
  return (
    <img
      className={classes.Icon}
      src={showPass ? eye : eyeCrossed}
      alt="Toggle Visibility"
      onClick={toggleVisibility}
    />
  );
}

export default ToggleVisibilityIcon;
