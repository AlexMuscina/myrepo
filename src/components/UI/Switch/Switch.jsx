import React from "react";
import classes from "./Switch.module.scss";

function Switch({ isToggled, onToggle }) {
  return (
    <label className={classes.Switch} htmlFor="checkbox">
      <input
        checked={isToggled}
        onChange={onToggle}
        id="checkbox"
        type="checkbox"
      />
      <span className={classes.Slider} />
    </label>
  );
}

export default Switch;
