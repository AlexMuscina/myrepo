import React from "react";
import classes from "./Button.module.scss";

function Button({ text, bgColor, border, color, width, weight, icon, click }) {
  return (
    <button
      onClick={click}
      className={classes.Button}
      style={
        // Check if we have border.
        // If we have we use one type of style if not another
        border
          ? {
              backgroundColor: bgColor,
              border: `1px solid ${color}`,
              color: color,
              width: width,
              fontWeight: weight,
            }
          : {
              backgroundColor: bgColor,
              border: "none",
              color: color,
              width: width,
              fontWeight: weight,
            }
      }
    >
      {icon && <img className={classes.Img} src={icon} alt={text} />}
      {text}
    </button>
  );
}

export default Button;
