import React from "react";
import logo from "../../../assets/images/logo-white.svg";
import { Link } from "react-router-dom";

function Logo() {
  return (
    <Link to="/">
      <img src={logo} alt="GuidefAi" />
    </Link>
  );
}

export default Logo;
