import React, { useState } from "react";
import Card from "../UI/Card/Card";
import Button from "../UI/Button/Button";
import classes from "./CreateAccountForm.module.scss";
import { Link } from "react-router-dom";
import variables from "../../utilities/color-variables.module.scss";
import Step1 from "../Step1/Step1";

function CreateAccountForm() {
  const [showPass, setShowPass] = useState(false);
  const [form, setForm] = useState({
    email: "",
    fName: "",
    lName: "",
    password: "",
    privacyPolicy: true,
    companyName: "",
  });
  const [count, setCount] = useState(1);

  const updateForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  return (
    <Card>
      <div className={classes.MultiStepForm}>
        <h5 className={classes.H5}>
          <span style={{ fontWeight: "bold" }}>Step {count}:</span> Account
          information
        </h5>
      </div>
      <h1 className="h1">Create an account</h1>
      <div style={{ marginBottom: "4rem" }} className={classes.Row}>
        <h5 className={classes.H5}>Already have an account?</h5>
        <Link to="/" className={classes.LinkH5}>
          Sign in
        </Link>
      </div>
      <form
        className={classes.Form}
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        {count === 1 && (
          <Step1
            form={form}
            setForm={setForm}
            updateForm={updateForm}
            showPass={showPass}
            setShowPass={setShowPass}
          />
        )}
        <div className={classes.Row} style={{ marginTop: "6rem" }}>
          <Button
            text="Next step"
            bgColor={variables.transparent}
            color={variables.blue1}
            border
            width="initial"
            weight="bold"
            click={() => setCount(count + 1)}
          />
        </div>
      </form>
    </Card>
  );
}

export default CreateAccountForm;
