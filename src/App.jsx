import LandingPage from "./components/LandingPage/LandingPage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/" component={LandingPage} />
          <Route />
        </Switch>
      </Router>
    </>
  );
}

export default App;
